<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/clear', function () {
    Cache::flush();
    session()->flush();
    echo "cleared";
});

Route::get('/', function () {
    return view('front-end/index');
});

Route::get('/logins', function () {
    return view('front-end/login');
});

Route::get('/profile', function () {
	if (!Auth::check()) {
		return view('front-end/index');
	}else{
    	return view('front-end/profile');
	}
});



Route::group([],function(){
	Route::post('users/login',['uses'=>'UserLoginController@login']);
	Route::get('logout',['middleware' => 'admin','uses'=>'UserLoginController@logout']);
	Route::post('users/userDetails',['uses'=>'UserLoginController@getUserDetails']);
	Route::post('users/update',['uses'=>'UserLoginController@update']);

	
});
