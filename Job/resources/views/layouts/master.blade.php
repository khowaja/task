<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Lorem Ipsum</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ url('css/app.css') }}"/>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
    <script type="text/javascript" src="{{ url('js/event.js') }}"></script> 
</head>
<?php
  if(Session::get('id')){
?>  
<body onload="CalluserInfo()">
<?php}else{ ?>
<body> <?php }?>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top">
        <a class="navbar-brand" href="{{URL::to('/')}}">Lorem <img class="logo" src="img/logo.png" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="main-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
             <?php if(Session::get('id')){?>
                <a class="nav-link" href="{{URL::to('/logout')}}">Logout</a>
                <?php } else {?>
                    <a class="nav-link" href="{{URL::to('/logins')}}">Login</a>
                <?php } ?>
          </li>
        </ul>
      </div>
    </nav>

@yield('content')

  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"  ></script>
    
    <script type="text/javascript" src="{{ url('js/main.js') }}"></script> 
    <?php if(Session::get('id')){?>
        <input type="hidden" id="onlineUserId" value="<?php echo Session::get('id'); ?>">
    <?php }?>
</body>

</html>