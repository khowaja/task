@extends('layouts.master')
@section('content')

<header class="py-5 bg-image-full" style="background-image: url('img/banner2.jpg');">
        <div class="container-fluid">
            <div class="row  justify-content-end">
                <div class="col-md-4">
                    <div class="text-area">
                       <h1 class="text-right">User Profile</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main role="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-container">
                    <span>Welcome: <strong id="nameSurname"></strong></span>
                        <form>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="userName" aria-describedby="emailHelp" placeholder="Enter name">
                        </div>
                         <div class="form-group">
                            <label>Surname</label>
                            <input type="text" class="form-control" id="surName" aria-describedby="" placeholder="Enter surname">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" id="emailadress" aria-describedby="emailHelp" disabled="" placeholder="Enter email">
                        </div>
                        <a id="innerPageDetails"  class="btn btn-primary">Submit</a>
                        <p id="showMessageUpdate"></p>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection