@extends('layouts.master')
@section('content')
<header class="py-5 bg-image-full" style="background-image: url('img/banner2.jpg');">
        <div class="container-fluid">
            <div class="row  justify-content-end">
                <div class="col-md-4">
                    <div class="form-container">
                        <form>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <p id="showMessage"></p>
                        <a id="login" class="btn btn-primary login-btn float-right">Login</a>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </header>

@endsection