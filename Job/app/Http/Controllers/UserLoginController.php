<?php

namespace Job\Http\Controllers;

use Illuminate\Http\Request;
use Job\Data\Repositories\UserLoginRepository;
use Job\Data\Models\User;
use \Validator, \Input;
use Illuminate\Http\Response;
use \Auth,\URL,\Session,\View,\Redirect;

class UserLoginController extends Controller
{
    public function __construct(UserLoginRepository $userRepo) {
        $this->_repository = $userRepo;
    }
    public function login(Request $request){

        $input = $request->only('email','password');
        $rules = [ 
            'email' => 'email',
        ];
        $messages = [];
        $validator = Validator::make( $input, $rules, $messages);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code,'messages' => $validator->messages()->all()]];
        }else{

            $data = $this->_repository->login($input); 

            if($data == null && $data == NULL){
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['Email is not registered.']]]; 
            }else if($data == 'invalidPassword'){
                $code = 400;
                $output = ['error' => ['code' => $code,'messages' => ['Password is invalid.']]]; 
            }
            else if($data == 'invalidEmail'){
                $code = 400;
                $output = ['error' => ['code' => $code,'messages' => ['Email is invalid.']]]; 
            }else{
                unset($data->password);
                Auth::loginUsingId($data->id);
                Session::put('id',$data->id);
                $arr = array('id'=>$data->id);
                $userdata = $this->_repository->userInfo($arr);
                $userInfoArr = [];
                $userInfoArr['id']          = $userdata->id;
                $userInfoArr['name']        = $userdata->name;
                $userInfoArr['surname']    = $userdata->surname;
                $userInfoArr['email']       = $userdata->email;
                $code = 200;
                $output = ['response' => ['code' => $code,'messages' => ['You have been logged in successfully.'], 'userInfo' => $userInfoArr]]; 
                /*return View('front-end.profile', ['userInfo' => $userInfoArr]);*/
                // return view::make('front-end/profile', ['userInfo' => $userInfoArr]);
            }
        }
        return response()->json($output, $code);
 
    }

    public function logout(Request $request) {
        Auth::logout();
        /*Cache::flush();*/
        Session::flush();
        $request->session()->flush();
        return redirect('/');
    }
    public function getUserDetails (Request $request){
    	$input = $request->only('id');
    	$arr = array('id'=>$input['id']);

    	$data = $this->_repository->userInfo($arr);
    	 if($data == null && $data == NULL){
            $code = 404;
            $output = ['error' => ['code' => $code,'messages' => ['Email is not registered.']]]; 
        }else{
        	$code = 200;
        	$output = ['response' => ['code' => $code,'messages' => ['data fetched successfully.'], 'userInfo' => $data]];
        }
        return $output;
    }

    public function update(Request $request){
        $input = $request->only('id','user','surname','email');
        $rules = [];
        $messages = [];
        $validator = Validator::make( $input, $rules, $messages);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code,'messages' => $validator->messages()->all()]];
        }else{

            $data = $this->_repository->update($input); 
            if($data == null && $data == NULL){
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['There is no data updated.']]]; 
            }else{
                $code = 200;
                $output = ['response' => ['data'=>$data, 'code' => $code,'messages' => ['data updated successfully.']]]; 
            }
        }
        return response()->json($output, $code);

    }
}
