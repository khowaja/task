<?php 
namespace Job\Data\Repositories;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use Carbon\Carbon;

/*Calling all models*/
use Job\Data\Models\User;
use Job\Data\Models\UserRole;

use \StdClass;



class UserLoginRepository {

	private $model;
	private $_cacheKey = 'user';

	public function __construct(User $users) {
		$this->model = $users;
	}

	public function update(array $data = []) {
		$user = $this->model->find($data['id']);
		if ($user != NULL) {
			if (isset($data['user']) && $data['user'] != '') {
				$user->name = $data['user'];
			}
			if (isset($data['surname']) && $data['surname'] != '') {
				$user->surname = $data['surname'];
			}
			
			if (isset($data['email']) && $data['email'] != '') {
				$user->email = $data['email'];
			}
			
			$user->updated_at = Carbon::now();

			if ($user->save()) {
				$data = $this->userInfo(array('id'=>$data['id']));
				return $data;
			} else {
				return false;
			}
		} else {
			return NULL;
		}
	}
	public function deleteById($id) {
		$user = $this->model->find($id);
		if ($user != NULL) {
			if($user->delete()){
				return true;
			} else {
				return false;
			}
		} else {
			return NULL;
		}
	}
	public function login(array $input = []){
		
		$checkEmail = User::where('email','=',$input['email']);
		if($checkEmail->count() > 0){
			if (Hash::check($input['password'], $checkEmail->first()->password)) {
					unset($checkEmail->first()->password);
					return $checkEmail->first();
				} else {
					return 'invalidPassword';
				}
		}else
		{
			return "invalidEmail";
		}
	}

	public function userInfo(array $input = []){
		$user = new User;
		$getData = User::where('id','=',$input['id'])->first();
		if($getData){
			return $getData;
		}else return false;
	}


}

?>