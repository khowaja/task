var Task = Task || {}; // "If Task is not defined, make it equal to an empty object"
Task.App = Task.App || {};
Task.Config = Task.Config || {};

Task.Config = (function(){

	if(window.location.host == 'localhost'){

		var apiUrl = window.location.protocol+'//'+window.location.host+'/Job/public';
		var localUrl = window.location.protocol+'//'+window.location.host+'/Job/public';
	

	} 

	var getApiUrl = function(){
		return apiUrl;
	};
	var getAppUrl = function(){
		return appUrl;
	};
	var getImageUrl = function(){
		return imageUrl;
	};
	var getAssetUrl = function(){
		return assetUrl;
	};
	var getLocalUrl = function(){
		return localUrl;
	};

	return {
		getApiUrl:getApiUrl,
		getAppUrl:getAppUrl,
		getImageUrl:getImageUrl,
		getAssetUrl:getAssetUrl,
		getLocalUrl:getLocalUrl
	}

}());

Task.App = (function () {

	var config = Task.Config;
	var init = function () {
		if (!window.console) window.console = {log: function(obj) {}};
		console.log('Application has been initialized...');
	};

	return {
			init:init
	};
}());

Task.App.UserCredentials = (function(){
	var config = Task.Config;
	var userApiUrl = config.getApiUrl()+'/users';	

	var login = function(){ 

		var errors = [];

		var email = $('#email').val();
		var password = $('#password').val();
		var errors = [];

		if ($.trim(email) == ''){
			errors.push('Please enter email');
			$('#email').addClass('has-error');
		}else if (!Task.App.Utils.isValidEmail($.trim(email))) {
				errors.push('Please enter valid email');
				$('#email').addClass('has-error');	
		}else {
			$('#email').removeClass('has-error')
		}

		if ($.trim(password) == ''){
			errors.push('Please enter password');
			$('#password').addClass('has-error');
		} else {
			$('#password').removeClass('has-error')
		}


		//convert into JSON
		var jsonData = {
			email: $.trim(email),
			password: $.trim(password),
		}

		if(errors.length < 1){

			var request = $.ajax({

				url: userApiUrl+'/login',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});

			request.done(function(data){
				if(data.response.code == 200){
					$("#showMessage").html(data.response.messages[0]).removeClass('alert alert-danger').addClass('alert alert-success').fadeIn().delay(3000).fadeOut('slow',function(){	
						window.location.href = config.getLocalUrl() +'/profile';
					});					
				}
			});

			request.fail(function(jqXHR, textStatus){
				var jsonResponse = $.parseJSON(jqXHR.responseText);				
				$("#showMessage").html(jsonResponse.error.messages[0]).addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
			});
		}
	}

	var userInfo = function(id){
		var jsonData = {
			id: $.trim(id),
		}
		var request = $.ajax({
			url: userApiUrl+'/userDetails',
			data: jsonData,
			type: 'POST',
			dataType:'json'
		});
		request.done(function(data){
			if(data.response.code == 200){
			 $('#userName').val(data.response.userInfo.name);
			 $('#surName').val(data.response.userInfo.surname);
			 $('#emailadress').val(data.response.userInfo.email);
			 $('#nameSurname').html(data.response.userInfo.name + " " + data.response.userInfo.surname);									
			}
		});
	}

	var updateUser = function(){
		var user 	=  	 $('#userName').val();
		var surname = 	 $('#surName').val();
		var email 	= 	 $('#emailadress').val();
		var id 		= 	 $('#onlineUserId').val();
		var errors = [];

		if ($.trim(email) == ''){
			errors.push('Please enter email');
			$('#emailadress').addClass('has-error');
		}else if (!Task.App.Utils.isValidEmail($.trim(email))) {
				errors.push('Please enter valid email');
				$('#emailadress').addClass('has-error');	
		}else {
			$('#emailadress').removeClass('has-error')
		}

		if ($.trim(user) == ''){
			errors.push('Please enter username');
			$('#userName').addClass('has-error');
		}else{
			$('#userName').removeClass('has-error')
		}

		if ($.trim(surname) == ''){
			errors.push('Please enter surname');
			$('#surName').addClass('has-error');
		}else{
			$('#surName').removeClass('has-error')
		}

		var jsonData = {
			email: $.trim(email),
			user: $.trim(user),
			surname: $.trim(surname),
			id: $.trim(id),
		}

		if(errors.length < 1){

			var request = $.ajax({

				url: userApiUrl+'/update',
				data: jsonData,
				type: 'POST',
				dataType:'json'
			});

			request.done(function(data){
				if(data.response.code == 200){
					$("#showMessageUpdate").html('Information updated').addClass('alert alert-success').fadeIn().delay(3000).fadeOut();							
					$('#nameSurname').html(data.response.data.name + " " + data.response.data.surname);
				}
		});

		}




	}


	return {
		login:login,
		userInfo:userInfo,
		updateUser:updateUser,
	};
}());


Task.App.Utils = (function(){	

	var isValidEmail = function(email){
		var emailexp = /^([a-zA-Z0-9])([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|([a-zA-Z0-9\-\.]+))\.([a-zA-Z]{2,6}|[0-9]{1,6})(\]?)$/;
		if (emailexp.test(email.replace(/\s/g,""))== false) {
			return false;
		}else{
			return true;
		}
    }


	return {
		isValidEmail:isValidEmail,
	};

}());