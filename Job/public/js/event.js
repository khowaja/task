// front
function CalluserInfo() {
	var id = $('#onlineUserId').val();
    Task.App.UserCredentials.userInfo(id);
}

$(function () {	

	$('#login').click(function(){
		Task.App.UserCredentials.login();
	});

	$(document).keypress(function (e) {
	    if (e.which == 13) {
	        Task.App.UserCredentials.login();
	    }
	});

	$('#innerPageDetails').click(function(){
		Task.App.UserCredentials.updateUser();
	});


});

