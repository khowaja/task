1. This task is made on Bootstrap and SASS that works on any device. 

2. If user do not enter valid email and password it gives error. Session is used in this project because we need to get the information of user and populated in the fields which are in profile page.

3. Once user update the information it shows the new information without reloading the profile page.

4. If user can press the back button or the url of profile after logout, it will not redirect to profile page. It shows the index page.

There is database with the name of task.sql. 

Database name is task too. for more information check .env file

Thank you. Have a wonderful day!